package net.oschina.git.entity;

import java.util.List;

public class Repositories {
	private String name;
	private List<String> repositories;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getRepositories() {
		return repositories;
	}

	public void setRepositories(List<String> repositories) {
		this.repositories = repositories;
	}
	
}
